﻿using System;
using System.Web.Mvc;
using System.Web.Routing;

namespace JAG.ClickToCall
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
            routes.IgnoreRoute(String.Empty);

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Click", action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}