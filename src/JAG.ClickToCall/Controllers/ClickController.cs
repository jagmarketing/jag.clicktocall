﻿using JAG.Common.MVC;
using JAG.Common.MVC.ViewModel;
using JAG.Common.MVC.ViewModel.Dripple;
using log4net;
using System;
using System.Web.Mvc;

namespace JAG.ClickToCall.Controllers
{
    /// <summary>
    /// Caters for both click to call and click to form.
    /// </summary>
    public class ClickController : Controller
    {
        private ILog _Log = LogManager.GetLogger(typeof(ClickController));

        /// <summary>
        /// Processes query string parameters for click to call and click to form.
        /// </summary>
        /// <returns></returns>
        public ActionResult Process()
        {
            try
            {
                #region Handles leads coming from Dripple.

                //Since we not rendering any view from this website, pass empty view name, 
                //and dummy model that inherits from the base model.
                Common.MVC.ClickToCall.Process(Request, String.Empty, new CarInsuranceViewModel(), out string view, out object model, out object process);

                #endregion

                //Check if it is click to call or click to form.
                string clickToCall = Request.QueryString[Constants.IsClickToCall];

                //Get the website to redirect to.
                string redirectTo = Request.QueryString[Constants.RedirectTo];

                if (!String.IsNullOrWhiteSpace(clickToCall) && Boolean.TryParse(clickToCall, out bool isClickToCall) && isClickToCall)
                {
                    //We are doing a click to call.
                    //By this point, the lead has been successfully submitted.
                    //We then need to redirect to the desired website.
                    return Redirect(redirectTo);
                }
                else
                {
                    //We are doing a click to form.
                    ProcessClickToCall response = (ProcessClickToCall)process;

                    //We then need to redirect to the desired website,
                    //populating the customer details.
                    string query = $"{redirectTo}?first_name={response.Customer.FirstName.Trim()}&" +
                                   $"surname={response.Customer.LastName.Trim()}&" +
                                   $"contact_number={response.Customer.CellNumber.Trim()}&" +
                                   $"email_address={response.Customer.Email.Trim()}";

                    return Redirect(query);
                }
            }
            catch(Exception exception)
            {
                _Log.Error($"Error while trying to redirect to WordPress site. {exception.Message} Request was {Request.Url.Query}", exception);

                return new FilePathResult("~/Index.html", "text/html");
            }
        }
    }
}